local name, args = ...

local A = LibStub("AceAddon-3.0"):NewAddon(name, "AceConsole-3.0", "AceEvent-3.0", "AceTimer-3.0", "AceHook-3.0")

args[1] = A

local CreateFrame = CreateFrame

function A:OnInitialize()
	self.db = LibStub("AceDB-3.0"):New(name.."DB", self.defaults, true)
end

function A:OnEnable()

end
