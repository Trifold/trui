local A = unpack(select(2, ...))

local frame = CreateFrame("Frame")
frame:RegisterEvent("UNIT_NAME_UPDATE")
frame:RegisterEvent("GROUP_ROSTER_UPDATE")
frame:SetScript("OnEvent", function(self, event, ...)
	for i = 1, 5 do
		local unit = "party"..i
		if (UnitExists(unit)) then
			local guid = UnitGUID(unit)
			print(event, UnitName(unit), guid)
		end
	end
end)

--[[

	format = "[color:class][health:current]"
	text = "|cFFFF00FF250000|r"

	fs.class = "Paladin"

	if (fs.class ~= unitClass) then
		-- change
	end

	local funcs = {
		["curhp"] = function(self) return self.health end,
		["perhp"] = function(self) return self.health > 0 and floor(self.health / self.health * 100 + .5) or 0 end,
		["curhp:short"] = function(self) return A:ShortenValue(self.health, 2) end
	}

	health.OnUpdate = function(self, unit, min, max)
		frame.health = min
		frame.healthMax = max
		A:UpdateTags(unit, "UNIT_HEALTH")
	end
]]
