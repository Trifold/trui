local A = unpack(select(2, ...))

local function diff(s, t)
    local c = setmetatable({}, { __index = s })
    for k,v in next, s do
        if (type(v) == "table") then
            c[k] = setmetatable(diff(v, t[k]), { __index = v })
        else
            if (t[k] ~= v) then
               c[k] = t[k]
            end
        end
    end
    return c
end

local function conf(str)
    local c = {}
    for w in str:gmatch("[^%s]+") do 
        table.insert(c, w) 
    end
    return c
end