local A = unpack(select(2, ...))

local format = string.format

function A:ShortenValue(value, decimals)
    if value > 1e9 then
        return format("%."..decimals.."f", value/1e9).."B"
    elseif value > 1e6 then
        return format("%."..decimals.."f", value/1e6).."M"
    elseif value > 1e3 then
        return format("%."..decimals.."f", value/1e3).."K"
    else
        return value
    end
end
